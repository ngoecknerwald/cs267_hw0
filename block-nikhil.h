#ifndef BLOCK_NIKHIL
#define BLOCK_NIKHIL

/* This auxiliary subroutine performs a smaller dgemm operation
 *  C := C + A * B
 * where C is M-by-N, A is M-by-K, and B is K-by-N. */
void do_block (int lda, int M, int N, int K, double* A, double* B, double* C);

#endif
