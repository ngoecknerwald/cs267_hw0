#!/bin/sh

#######################################################################
#
# Parameters to sweep over, the L2, L1 and transpose block sizes
#
# Experiment with these variables and flags!
#
#######################################################################
#Shell lists of the form "100 80 60" etc.
L2s="128 196 256 320 384"
L1s="64"
TRANSs="128"
# GCC FLAGS
OPT="-O3 -Ofast -ffast-math -funroll-loops -march=corei7-avx -funsafe-loop-optimizations -fipa-cp-alignment"
CFLAGS="-Wall -std=gnu99 $OPT"
#Intel Flags
#OPT="-O3"
#CFLAGS="-Wall -std=gnu99 $OPT -h vector3 -xAVX"
LDFLAGS="-Wall"

#######################################################################
#
# Variables needed to run the generation script
#
# Changing anything below here is likely to break things!
#
#######################################################################
L2_0="180" #Initial L2 block size in constants.h
L1_0="60" #Initial L1 block size in constants .h
TRANS_0="70" #Initial transpose block size
benchmark="benchmark.c" #Name of the benchmark c file
header="block-nikhil.h transpose.h" #The relevant header files
dgemm="block-nikhil.c dgemm-double-block.c transpose.c" #The relevant .c files
clean="block-nikhil.o dgemm-double-block.o transpose.o" #The relevant .o files
constants="constants.h" #Name of the constants file in main directory
CC="cc"
LDLIBS="-lrt" # librt is needed for clock_gettime
jobfile="job-production"


#######################################################################
#
# Job generation code! Modify at your own risk
#
# Files are named according to the convention
#
# test_L1_L2_TRANS
#
#######################################################################
#Copy the necessary files over
for f in $dgemm; do cp ../$f .; done
for f in $header; do cp ../$f .; done

#Copy the benchmark file
cp ../$benchmark .

#Build the benchmark .o file
if [ ! -e benchmark.o ]
then
  execstring="$CC $CFLAGS -c $benchmark -o benchmark.o"
  echo $execstring; eval $execstring
fi

#Compile all of the job files
function build_job {

  #Read in variables passed to shell functions
  L1l=$1; L2l=$2; TRANSl=$3

  #Generate the directory for the executable
  workdir="test_${L1l}_${L2l}_${TRANSl}"
  if [ -e $workdir ]
  then
    rm $workdir/*
  else
    mkdir $workdir
  fi

  #Copy the constants file down
  cp ../$constants .
  sed -i "s/$L1_0/$L1l/g" $constants
  sed -i "s/$L2_0/$L2l/g" $constants
  sed -i "s/$TRANS_0/$TRANSl/g" $constants
  #Compile the dgemm method
  execstring="$CC $CFLAGS -c $dgemm"
  echo $execstring;  eval $execstring
  #Link everything
  execstring="$CC $LDFLAGS -lrt *.o -o benchmark-production"
  echo $execstring;  eval $execstring

  #Clean everything up
  for f in $clean; do rm $f; done
  mv $constants $workdir
  mv "benchmark-production" $workdir

  #Generate the batch submission script
  cp $jobfile $workdir
  sed -i "s/FOLDER/${L1l}_${L2l}_${TRANSl}/g" $workdir/$jobfile

}

#Iterate over the dgemm methods
for L1 in $L1s; do
  for L2 in $L2s; do
    for TRANS in $TRANSs; do
      if test $L2 -gt $L1; then
        build_job $L1 $L2 $TRANS
      fi
    done
  done
done

#Do the final clean-up
rm *.h
rm *.c
rm *.o
