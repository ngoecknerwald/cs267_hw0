#include <immintrin.h> // For: AVX instructions

#ifndef TRANSPOSE
#define TRANSPOSE

/*
 * Method to transpose data from one matrix into a padded transpose
 * Assumes non-padded matrix *mat with size n*n*sizeof(double)
 * Assumes padded matrix that is the result of the call:
 * mat_trans_pad = (double *) calloc( npad * npad * sizeof(double))
 */
void copy_transpose (int n, int npad, double* mat, double* mat_trans_pad);

void copy_transpose_blocked (int n, int npad, double* mat, double* mat_trans_pad);

/*
 * Method to copy data from one matrix into a padded copy
 * Assumes non-padded matrix *mat with size n*n*sizeof(double)
 * Assumes padded matrix that is the result of the call:
 * mat_pad = (double *) calloc( npad * npad * sizeof(double))
 */
void copy_to_pad (int n, int npad, double* mat, double* mat_pad);

/*
 * The same thing but in reverse
 */
void copy_from_pad (int n, int npad, double* mat, double* mat_pad);

/*
 * Efficiently transpose matrix block
 */
void block_transpose(double *A, double *B, int n, int n_pad);

#endif
