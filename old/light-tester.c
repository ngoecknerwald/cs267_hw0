#include <stdlib.h> // For: exit, drand48, malloc, free, NULL, EXIT_FAILURE
#include <stdio.h>  // For: perror
#include <string.h> // For: memset
#include <float.h>  // For: DBL_EPSILON
#include <math.h>   // For: fabs
#include "transpose.h"

#define GETTIMEOFDAY
#ifdef GETTIMEOFDAY
#include <sys/time.h> // For struct timeval, gettimeofday
#else
#include <time.h> // For struct timespec, clock_gettime, CLOCK_MONOTONIC
#endif

#define MAX_SPEED 19.2 // defining Max Gflops/s per core on Edison.

//dgemm to test!
extern void square_dgemm (int, double*, double*, double*);

void die (const char* message)
{
  perror (message);
  exit (EXIT_FAILURE);
}

void print_mat(double* mat, int n)
{
  for(int i = 0; i<n; ++i)
  {
  printf("| ");
    for(int j =0; j<n; ++j)
      printf("%f\t",mat[i*n + j]);
  printf("|\n");
  }
  printf("\n");
}

void subtract_ref(double *mat, double * mat_ref, int n)
{
  for(int i = 0; i<n; ++i)
    for(int j =0; j<n; ++j)
      mat[i*n + j] -= mat_ref[i*n + j];
}


void fill (double* p, int n)
{
  for (int i = 0; i < n; ++i)
    p[i] = 2 * drand48() - 1; // Uniformly distributed over [-1, 1]
}

void ref_square_dgemm (int n, double* A, double* B, double* C)
{
  /* For each row i of A */
  for (int i = 0; i < n; ++i)
    /* For each column j of B */
    for (int j = 0; j < n; ++j)
    {
      /* Compute C(i,j) */
      double cij = C[i+j*n];
      for( int k = 0; k < n; k++ )
        cij += A[i+k*n] * B[k+j*n];
      C[i+j*n] = cij;
    }
}

/* The benchmarking program */
int main (int argc, char **argv)
{

  int n = 115;

  /* allocate memory for all problems */
  void * buf = NULL;
  void * buf_ref = NULL;
  buf_ref = (double*) malloc (3 * n * n * sizeof(double));
  buf = (double*) malloc (3 * n * n * sizeof(double));

  /* Create pointers to the reference and final matrices */
  double* A_ref = buf_ref + 0;
  //double* B_ref = A_ref + n*n;
  //double* C_ref = B_ref + n*n;
  double* A = buf + 0;
  //double* B = A + n*n;
  //double* C = B + n*n;

  /* Fill things */
  fill (A, n*n);
  //fill (B, n*n);
  //fill (C, n*n);

  // Testing the copy and padding code
  memcpy(buf_ref, buf, 3*n*n*sizeof(double));

  int n_pad = 116;
  /* Allocated padded matrices */
  double* A_pad = (double*)calloc(n_pad * n_pad, sizeof(double));
  double* A_pad_ref = (double*)calloc(n_pad * n_pad, sizeof(double));
  //double* B_pad = (double*)calloc(n_pad * n_pad, sizeof(double));
  //double* C_pad = (double*)calloc(n_pad * n_pad, sizeof(double));

  /* Transpose A matrix */
  copy_transpose(n, n_pad, A_ref, A_pad_ref);
  copy_transpose_blocked(n, n_pad, A, A_pad);

  printf("Reference\n\n");
  //print_mat(A_pad_ref, n_pad);
  printf("Blocked\n\n");
  //print_mat(A_pad, n_pad);

  printf("Difference\n\n");
  subtract_ref(A_pad, A_pad_ref, n_pad);
  print_mat(A_pad, n_pad);


  /* Copy but do not transpose B, C */
  //copy_to_pad(n, n_pad, B, B_pad);
  //copy_to_pad(n, n_pad, C, C_pad);
  //print_mat(A, n);
  //print_mat(A_pad, n_pad);
  //print_mat(B, n);
  //print_mat(B_pad, n_pad);
  //copy_from_pad(n, n_pad, C, C_pad);
  //print_mat(C, n);
  //print_mat(C_pad, n_pad);

  /*memcpy(buf_ref, buf, 3*n*n*sizeof(double));

  square_dgemm(n, A, B, C);

  ref_square_dgemm(n, A_ref, B_ref, C_ref);
 
  print_mat(C,n);
  print_mat(C_ref,n);*/
 
  free (buf);
  free (buf_ref);

  return 0;
}
