//Including library to do power operation...allowed to do this?
#include <math.h>
#include <stdio.h>
#include "constants.h"

const char* dgemm_desc = "Bill blocked dgemm.";

//#if !defined(L1_BLOCK)
//#define L1_BLOCK 60
//#endif

//#if !defined(L2_BLOCK)
//#define L2_BLOCK 180
//#endif

//#if !defined(BITS_IN_FLOAT)
//#define BITS_IN_FLOAT 64
//#endif

//L1_size in KB
//#if !defined(L1_SIZE)
//#define L1_SIZE 32
//#endif

//L2_size in KB
//#if !defined(L2_SIZE)
//#define L2_SIZE 256
//#endif

//number of bits in a KB: 8*1024
//#if !defined(CONVERT_TO_KB)
//#define CONVERT_TO_KB 8192
//#endif

//#define min(a,b) (((a)<(b))?(a):(b))


/* This auxiliary subroutine performs a smaller dgemm operation
 *  C := C + A * B
 * where C is M-by-N, A is M-by-K, and B is K-by-N. */
static void do_block(int lda, int M, int N, int K, double* A, double* B, double* C)
{
  /* For each row i of A */
  for (int i = 0; i < M; ++i)
    /* For each column j of B */ 
    for (int j = 0; j < N; ++j) 
    {
      /* Compute C(i,j) */
      double cij = C[i+j*lda];
      for (int k = 0; k < K; ++k)
        cij += A[i+k*lda] * B[k+j*lda];
        C[i+j*lda] = cij;
    }
}

void block_L1(int lda, double* A, double* B, double* C, int block_size, int M0, int N0, int K0,int i0, int j0, int k0) {
  /* For each block-row of A */ 
  for (int i = 0; i < M0; i += block_size)
    /* For each block-column of B */
    for (int j = 0; j < N0; j += block_size)
      /* Accumulate block dgemms into block of C */
      for (int k = 0; k < K0; k += block_size)
      {
        /* Correct block dimensions if block "goes off edge of" the matrix */
        int M = min (block_size, lda-i0-i);
        int N = min (block_size, lda-j0-j);
        int K = min (block_size, lda-k0-k);

        /* Perform individual block dgemm */
        do_block(lda, M, N, K, A + i + k*lda, B + k + j*lda, C + i + j*lda);
      }
}

void block_L2(int lda, double* A, double* B, double* C, int block_size) {

  //block_size_L1 = calc_block_size();
  int block_size_L1 = L1_BLOCK;
  /* For each block-row of A */ 
  for (int i = 0; i < lda; i += block_size)
    /* For each block-column of B */
    for (int j = 0; j < lda; j += block_size)
      /* Accumulate block dgemms into block of C */
      for (int k = 0; k < lda; k += block_size)
      {
        /* Correct block dimensions if block "goes off edge of" the matrix */
        int M = min (block_size, lda-i);
        int N = min (block_size, lda-j);
        int K = min (block_size, lda-k);

        /* Perform individual block dgemm */
        block_L1(lda,A + i + k*lda, B + k + j*lda, C + i + j*lda, block_size_L1,M,N,K,i,j,k);
      }
}
/* This routine performs a dgemm operation
 *  C := C + A * B
 * where A, B, and C are lda-by-lda matrices stored in column-major format. 
 * On exit, A and B maintain their input values. */  
void square_dgemm (int lda, double* A, double* B, double* C)
{

  //dim is the dimension of the padded nxn matrix
  int dim = lda;
  int block_size = 0;
  //Calculate how big storage of all 3 matrices are
  int num_bits = (3 * dim*dim) * BITS_IN_FLOAT; // Is it not easier/clearer to use sizeof(float)?
  int total_size = num_bits/(CONVERT_TO_KB);
  //printf("%d, %d, %d\n",dim, num_bits, total_size);
  if (total_size < L1_SIZE) {
    //dont do any blocking
    do_block(dim,dim,dim,dim,A,B,C);
  } 
  else if (total_size > L1_SIZE && total_size < L2_SIZE) {
    //only do L1 cache blocking
    //block_size = calc_block_size();
    block_size = L1_BLOCK;
    block_L1(dim,A,B,C,block_size,dim,dim,dim,0,0,0);
  } 
  else { //greater than L2 cache size
    //block_size = calc_block_size();
    block_size = L2_BLOCK;
    block_L2(dim,A,B,C,block_size);
  }
}
