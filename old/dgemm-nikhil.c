#include <stdio.h>  // For: printf
#include <immintrin.h> // For: AVX instructions
#include "constants.h"

const char* dgemm_desc = "Simple blocked dgemm.";

/*#if !defined(BLOCK_SIZE)
#define BLOCK_SIZE 40
#endif

#define STEP_SIZE 4*/

#define min(a,b) (((a)<(b))?(a):(b))

/* This auxiliary subroutine performs a smaller dgemm operation
 *  C := C + A * B
 * where C is M-by-N, A is M-by-K, and B is K-by-N. */
static void do_block (int lda, int M, int N, int K, double* A, double* B, double* C)
{
  /* For each row i of A, doing 4 at a time */
  int row_bound = (M / STEP_SIZE) * STEP_SIZE;
  for (int i = 0; i < row_bound; i += STEP_SIZE) {
    /* For each column j of B */
    for (int j = 0; j < N; ++j) {
      int k_bound = (K / STEP_SIZE) * STEP_SIZE;
      /* Block of 4 entries in C to compute */
      __m256d c_block = _mm256_loadu_pd(C+i+(j*lda));
      /* Computing 4x4 grid in single loop */
      for (int k = 0; k < k_bound; k += STEP_SIZE) {
	/* Different columns of A */
	__m256d a1 = _mm256_loadu_pd(A+i+(k*lda));
	__m256d b1 = _mm256_set1_pd(B[k+j*lda]);
	c_block = _mm256_add_pd(c_block, _mm256_mul_pd(a1, b1));

	__m256d a2 = _mm256_loadu_pd(A+i+((k+1)*lda));
	__m256d b2 = _mm256_set1_pd(B[(k+1)+j*lda]);
	c_block = _mm256_add_pd(c_block, _mm256_mul_pd(a2, b2));

	__m256d a3 = _mm256_loadu_pd(A+i+((k+2)*lda));
	__m256d b3 = _mm256_set1_pd(B[(k+2)+j*lda]);
	c_block = _mm256_add_pd(c_block, _mm256_mul_pd(a3, b3));

	__m256d a4 = _mm256_loadu_pd(A+i+((k+3)*lda));
	__m256d b4 = _mm256_set1_pd(B[(k+3)+j*lda]);
	c_block = _mm256_add_pd(c_block, _mm256_mul_pd(a4, b4));
      }
      /* Edge cases */
      for (int k = k_bound; k < K; ++k) {
	__m256d a1 = _mm256_loadu_pd(A+i+(k*lda));
	__m256d b1 = _mm256_set1_pd(B[k+j*lda]);
	c_block = _mm256_add_pd(c_block, _mm256_mul_pd(a1, b1));
      }
      _mm256_storeu_pd(C+i+(j*lda), c_block);
    }
  }
  /* Last rows of A, edge cases */
  for (int i = row_bound; i < M; ++i) {
    /* For each column j of B */
    for (int j = 0; j < N; ++j) {
      /* Calc bound for loop unrolling */
      int k_bound = (K / STEP_SIZE) * STEP_SIZE;
      /* Compute C(i,j) */
      double cij = C[i+j*lda];
      /* Loop unrolling */
      for (int k = 0; k < k_bound; k += STEP_SIZE) {
	double s1 = A[i+k*lda] * B[k+j*lda];
	double s2 = A[i+(k+1)*lda] * B[(k+1)+j*lda];
	double s3 = A[i+(k+2)*lda] * B[(k+2)+j*lda];
	double s4 = A[i+(k+3)*lda] * B[(k+3)+j*lda];
	cij += s1 + s2 + s3 + s4;
      }
      /* Edge cases */
      for (int k = k_bound; k < K; ++k) {
	cij += A[i+k*lda] * B[k+j*lda];
      }
      C[i+j*lda] = cij;
    }
  }
}

/* This routine performs a dgemm operation
 *  C := C + A * B
 * where A, B, and C are lda-by-lda matrices stored in column-major format.
 * On exit, A and B maintain their input values. */
void square_dgemm (int lda, double* A, double* B, double* C) {
  /* For each block-row of A */
  for (int i = 0; i < lda; i += BLOCK_SIZE) {
    /* For each block-column of B */
    for (int j = 0; j < lda; j += BLOCK_SIZE) {
      /* Accumulate block dgemms into block of C */
      for (int k = 0; k < lda; k += BLOCK_SIZE) {
	/* Correct block dimensions if block "goes off edge of" the matrix */
	int M = min (BLOCK_SIZE, lda-i);
	int N = min (BLOCK_SIZE, lda-j);
	int K = min (BLOCK_SIZE, lda-k);

	/* Perform individual block dgemm */
	do_block(lda, M, N, K, A + i + k*lda, B + k + j*lda, C + i + j*lda);
      }
    }
  }
}
