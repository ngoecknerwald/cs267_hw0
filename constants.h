//Bill's L1 block size
#if !defined(L1_BLOCK)
#define L1_BLOCK 40
#endif

//Bill's L2 block size
#if !defined(L2_BLOCK)
#define L2_BLOCK 80
#endif

//Block size for the transpose code
#if !defined(BLOCK_SIZE)
#define BLOCK_SIZE 64
#endif

// Step size for A/C matrices
#define STEP_SIZE 8

// Step size for B matrices
#define STEP_SIZE_2 2

//Function macro to calculate minimum
#define min(a,b) (((a)<(b))?(a):(b))
