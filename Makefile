# on Edision we will benchmark you against the default vendor-tuned BLAS. The compiler wrappers handle all the linking. If you wish to compare with other BLAS implementations, check the NERSC documentation.
# This makefile is intended for the GNU C compiler. To change compilers, you need to type something like: "module swap PrgEnv-pgi PrgEnv-gnu" See the NERSC documentation for available compilers.

CC = cc
# GCC FLAGS
OPT = -O3 -Ofast -ffast-math -funroll-loops -march=corei7-avx -funsafe-loop-optimizations -fipa-cp-alignment
CFLAGS = -Wall -std=gnu99 $(OPT)
# ICC FLAGS
# OPT = -O3
# CFLAGS = -Wall -std=gnu99 $(OPT) -h vector3 -xAVX
LDFLAGS = -Wall
# librt is needed for clock_gettime
LDLIBS = -lrt

#targets = benchmark-blocked benchmark-nikhil benchmark-bill light-tester benchmark-naive
#objects = dgemm-blocked.o dgemm-nikhil.o dgemm-bill.o light-tester.o
targets = benchmark-blas benchmark-double-block benchmark-block-nikhil
objects = benchmark.o transpose.o block-nikhil.o dgemm-blas.o dgemm-double-block.o dgemm-block-nikhil.o


.PHONY : default
default : all

.PHONY : all
all : clean $(targets)

benchmark-block-nikhil: benchmark.o dgemm-block-nikhil.o transpose.o block-nikhil.o
	$(CC) -o $@ $^ $(LDLIBS)
benchmark-double-block: benchmark.o dgemm-double-block.o transpose.o block-nikhil.o
	$(CC) -o $@ $^ $(LDLIBS)
benchmark-blas : benchmark.o dgemm-blas.o
	$(CC) -o $@ $^ $(LDLIBS)
#light-tester : light-tester.o transpose.o dgemm-double-block.o block-nikhil.o
#	$(CC) -o $@ $^ $(LDLIBS)
#benchmark-naive : benchmark.o dgemm-naive.o
#       $(CC) -o $@ $^ $(LDLIBS)
#benchmark-blocked : benchmark.o dgemm-blocked.o
#       $(CC) -o $@ $^ $(LDLIBS)
#benchmark-nikhil: benchmark.o dgemm-nikhil.o
#       $(CC) -o $@ $^ $(LDLIBS)
#benchmark-bill: benchmark.o dgemm-bill.o
#       $(CC) -o $@ $^ $(LDLIBS)

%.o : %.c
	$(CC) -c $(CFLAGS) $<

.PHONY : clean
clean:
	rm -f $(targets) $(objects)
