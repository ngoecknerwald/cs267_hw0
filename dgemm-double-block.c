#include <stdlib.h> // For: posix_memalign
#include <string.h> // For: memset
#include "transpose.h" // For: tranpose + pad functions
#include "constants.h" // For preprocessor defined constants
#include "block-nikhil.h" //For the do_block method

const char* dgemm_desc = "Double blocked dgemm";

void block_L1(int lda, double* A, double* B, double* C, int block_size, int M0, int N0, int K0,int i0, int j0, int k0) {
  /* For each block-row of A */
  for (int k = 0; k < K0; k += block_size)
    /* For each block-column of B */
    for (int j = 0; j < N0; j += block_size)
      /* Accumulate block dgemms into block of C */
      for (int i = 0; i < M0; i += block_size)
      {
        /* Correct block dimensions if block "goes off edge of" the matrix */
        int M = min (block_size, lda-i0-i);
        int N = min (block_size, lda-j0-j);
        int K = min (block_size, lda-k0-k);

        /* Perform individual block dgemm */
        do_block(lda, M, N, K, A + i + (k*lda), B + k +( j*lda), C + i +(j*lda));
      }
}

void block_L2(int lda, double* A, double* B, double* C, int block_size) {

  //block_size_L1 = calc_block_size();
  int block_size_L1 = L1_BLOCK;
  /* For each block-row of A */
  for (int k = 0; k < lda; k += block_size)
    /* For each block-column of B */
    for (int j = 0; j < lda; j += block_size)
      /* Accumulate block dgemms into block of C */
      for (int i = 0; i < lda; i += block_size)
      {
        /* Correct block dimensions if block "goes off edge of" the matrix */
        int M = min (block_size, lda-i);
        int N = min (block_size, lda-j);
        int K = min (block_size, lda-k);

        /* Perform individual block dgemm */
        block_L1(lda,A + i + (k*lda), B + k +( j*lda), C + i +(j*lda), block_size_L1,M,N,K,i,j,k);
      }
}
/* This routine performs a dgemm operation
 *  C := C + A * B
 * where A, B, and C are lda-by-lda matrices stored in column-major format.
 * On exit, A and B maintain their input values. */
void square_dgemm (int lda, double* A, double* B, double* C) {
  int n_pad = lda + ((STEP_SIZE - (lda % STEP_SIZE)) % STEP_SIZE);
  /* Hacky fix to round up to new multiple of 8 */
  if (n_pad % 64 == 0) {
    n_pad += 8;
  }

  int num_elems = n_pad * n_pad;

  /* Allocated padded, memory aligned matrices */
 // double *A_pad = 0, *B_pad = 0, *C_pad = 0;
 // posix_memalign((void**)&A_pad, 32, num_elems*sizeof(double));
 // posix_memalign((void**)&B_pad, 32, num_elems*sizeof(double));
 // posix_memalign((void**)&C_pad, 32, num_elems*sizeof(double));

  double A_pad[num_elems] __attribute__((aligned(32)));
  double B_pad[num_elems] __attribute__((aligned(32)));
  double C_pad[num_elems] __attribute__((aligned(32)));


  /* Empty out memory */
  memset(A_pad, 0, num_elems*sizeof(double));
  memset(B_pad, 0, num_elems*sizeof(double));
  memset(C_pad, 0, num_elems*sizeof(double));

  /* Transpose A matrix */
 // copy_transpose_blocked(lda, n_pad, A, A_pad);

  /* Copy padded matrices */
  copy_to_pad(lda, n_pad, A, A_pad);
  copy_to_pad(lda, n_pad, B, B_pad);
  copy_to_pad(lda, n_pad, C, C_pad);
  if (n_pad < L2_BLOCK) {
    block_L1(n_pad,A_pad,B_pad,C_pad, L1_BLOCK, n_pad, n_pad, n_pad, 0,0,0);
  } else {
   //do_block(n_pad, n_pad, n_pad, n_pad, A_pad, B_pad, C_pad);
    block_L2(n_pad,A_pad, B_pad,C_pad, L2_BLOCK);
  }
  /* Copy calculated data back to C */
  copy_from_pad(lda, n_pad, C, C_pad);

  /* Free memory */
 // free(A_pad);
 // free(B_pad);
 // free(C_pad);
}
