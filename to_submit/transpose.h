#ifndef TRANSPOSE
#define TRANSPOSE

/*
 * Method to copy data from one matrix into a padded copy
 * Assumes non-padded matrix *mat with size n*n*sizeof(double)
 * Assumes padded matrix that is the result of the call:
 * mat_pad = (double *) calloc( npad * npad * sizeof(double))
 */
void copy_to_pad (int n, int npad, double* mat, double* mat_pad);

/*
 * The same thing but in reverse
 */
void copy_from_pad (int n, int npad, double* mat, double* mat_pad);

#endif
