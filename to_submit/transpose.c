#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "transpose.h"

/*
 * Method to copy data from one matrix into a padded matrix
 */
void copy_to_pad(int n, int npad, double* mat, double* mat_pad) {
  int num_elems = n*sizeof(double);
  for (int i = 0; i < n; ++i) {
    memcpy(mat_pad+(i*npad), mat+(i*n), num_elems);
  }
}

/*
 * The reverse
 */
void copy_from_pad(int n, int npad, double* mat, double* mat_pad) {
  int num_elems = n*sizeof(double);
  for (int i = 0; i < n; ++i) {
    memcpy(mat+(i*n), mat_pad+(i*npad), num_elems);
  }
}
