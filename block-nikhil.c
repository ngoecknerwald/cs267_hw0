#include <immintrin.h> // For: AVX instructions
#include "block-nikhil.h"
#include "constants.h" // For preprocessor defined constants

/* This auxiliary subroutine performs a smaller dgemm operation
 *  C := C + A * B
 * where C is M-by-N, A is M-by-K, and B is K-by-N. */
void do_block(int lda, int M, int N, int K, double* A, double* B, double* C) {
  /* For each col j of B, doing 2 at a time */
  for (int j = 0; j < N; j += STEP_SIZE_2) {
    /* For each row i of A, doing 8 at a time */
    for (int i = 0; i < M; i += STEP_SIZE) {
      /* Block of 8x2 entries in C to compute */
      __m256d c1 = _mm256_load_pd(C+i+(j*lda));
      __m256d c3 = _mm256_load_pd(C+i+((j+1)*lda));
      __m256d c2 = _mm256_load_pd(C+(i+4)+(j*lda));
      __m256d c4 = _mm256_load_pd(C+(i+4)+((j+1)*lda));

      /* Declare variables */
      __m256d a1, a2, a3, a4, b1, b2, b3, b4;

      /* Computing blocks of size 2 */
      for (int k = 0; k < K; k += STEP_SIZE_2) {
        /* Block of 4x2 entries in A, 2x2 entries in B */
        a1 = _mm256_load_pd(A+i+(k*lda));
        b1 = _mm256_set1_pd(B[k+(j*lda)]);
        c1 = _mm256_add_pd(c1, _mm256_mul_pd(a1, b1));

        a3 = _mm256_load_pd(A+i+((k+1)*lda));
        b2 = _mm256_set1_pd(B[(k+1)+(j*lda)]);
        c1 = _mm256_add_pd(c1, _mm256_mul_pd(a3, b2));

        b3 = _mm256_set1_pd(B[k+((j+1)*lda)]);
        a2 = _mm256_load_pd(A+(i+4)+(k*lda));
        c2 = _mm256_add_pd(c2, _mm256_mul_pd(a2, b1));

        b4 = _mm256_set1_pd(B[(k+1)+((j+1)*lda)]);
        a4 = _mm256_load_pd(A+(i+4)+((k+1)*lda));
        c2 = _mm256_add_pd(c2, _mm256_mul_pd(a4, b2));

        /* Aggregate 8x2 grid of C */
        c3 = _mm256_add_pd(c3, _mm256_add_pd(_mm256_mul_pd(a1, b3),
            _mm256_mul_pd(a3, b4)));
        c4 = _mm256_add_pd(c4, _mm256_add_pd(_mm256_mul_pd(a2, b3),
            _mm256_mul_pd(a4, b4)));
      }
      /* Store 8x2 grid in C */
      _mm256_store_pd(C+i+(j*lda), c1);
      _mm256_store_pd(C+i+((j+1)*lda), c3);
      _mm256_store_pd(C+(i+4)+(j*lda), c2);
      _mm256_store_pd(C+(i+4)+((j+1)*lda), c4);
    }
  }
}

