#include <stdlib.h> // For: posix_memalign
#include <string.h> // For: memset
#include <stdio.h> // For: printf
#include "transpose.h" // For: tranpose + pad functions
#include "constants.h" // For preprocessor defined constants
#include "block-nikhil.h" //For the do_block method

const char* dgemm_desc = "Double blocking with memory alignment";

/* Check if a number is a power of 2 */
inline static int power_two(int x) {
  return ((x != 0) && !(x & (x - 1)));
}

void print_matrix(double *mat, const int n) {
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      printf("%f ", mat[j+(i*n)]);
    }
    printf("\n");
  }
}

/* This routine performs a dgemm operation
 *  C := C + A * B
 * where A, B, and C are lda-by-lda matrices stored in column-major format.
 * On exit, A and B maintain their input values. */
void square_dgemm (int lda, double* A, double* B, double* C) {
  int n_pad = lda + ((STEP_SIZE - (lda % STEP_SIZE)) % STEP_SIZE);

  /* Hacky fix to round up to new multiple of 8 */
  if (n_pad % 64 == 0) {
    n_pad += 8;
  }

  int num_elems = n_pad * n_pad;

  /* Allocated padded, memory aligned matrices */
  double A_pad[num_elems] __attribute__((aligned(32)));
  double B_pad[num_elems] __attribute__((aligned(32)));
  double C_pad[num_elems] __attribute__((aligned(32)));

  /* Empty out memory */
  memset(A_pad, 0, sizeof(A_pad));
  memset(B_pad, 0, sizeof(B_pad));
  memset(C_pad, 0, sizeof(C_pad));

  /* Copy padded matrices */
  copy_to_pad(lda, n_pad, A, A_pad);
  copy_to_pad(lda, n_pad, B, B_pad);
  copy_to_pad(lda, n_pad, C, C_pad);

  int block_size = 32;

  /* Accumulate block dgemms into block of C */
  for (int k = 0; k < n_pad; k += block_size) {
    /* For each block-column of B */
    for (int j = 0; j < n_pad; j += block_size) {
      /* For each block-row of A */
      for (int i = 0; i < n_pad; i += block_size) {
        /* Correct block dimensions */
        int M = min(block_size, n_pad-i);
        int N = min(block_size, n_pad-j);
        int K = min(block_size, n_pad-k);

        /* Perform individual block dgemm */
        do_block(n_pad, M, N, K, A_pad+i+(k*n_pad), B_pad+k+(j*n_pad),
            C_pad+i+(j*n_pad));
      }
    }
  }

  /* Copy calculated data back to C */
  copy_from_pad(lda, n_pad, C, C_pad);
}
