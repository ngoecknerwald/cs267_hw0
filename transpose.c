#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <immintrin.h> // For: AVX instructions
#include "transpose.h"
#include "constants.h"

/*
 * Method to copy data from one matrix into a padded matrix
 */
void copy_to_pad(int n, int npad, double* mat, double* mat_pad) {
  int num_elems = n*sizeof(double);
  for (int i = 0; i < n; ++i) {
    memcpy(mat_pad+(i*npad), mat+(i*n), num_elems);
  }
}

/*
 * This auxiliary subroutine performs a smaller transpose operation on a off
 * diagonal sub matrix
 */
inline static void do_block_non_diagonal(int n, int npad, int block_size, int nrow, int ncol, double* mat_trans_pad)
{
  int imax=min(block_size, n - (ncol*block_size));
  int jmax=min(block_size, n - (nrow*block_size));
  for (int i = 0; i < imax; i++)
    /*Commented out explicit loop unrolling because it slowed things down. Also need to take care of boundary conditions*/
    for (int j = 0; j < jmax; j++/*=STEP_SIZE*/)
    {
      int colij=ncol*block_size+i;
      int rowji=ncol*block_size+i;

      int rowij0=nrow*block_size+j;
      int colji0=nrow*block_size+j;

      /*int rowij1=nrow*block_size+j+1;
      int colji1=nrow*block_size+j+1;

      int rowij2=nrow*block_size+j+2;
      int colji2=nrow*block_size+j+2;

      int rowij3=nrow*block_size+j+3;
      int colji3=nrow*block_size+j+3;*/

      double mij0 = mat_trans_pad[colij*npad+rowij0];
      double mji0 = mat_trans_pad[colji0*npad+rowji];
      mat_trans_pad[colij*npad+rowij0] = mji0;
      mat_trans_pad[colji0*npad+rowji] = mij0;

      /*double mij1 = mat_trans_pad[colij*npad+rowij1];
      double mji1 = mat_trans_pad[colji1*npad+rowji];
      mat_trans_pad[colij*npad+rowij1] = mji1;
      mat_trans_pad[colji1*npad+rowji] = mij1;

      double mij2 = mat_trans_pad[colij*npad+rowij2];
      double mji2 = mat_trans_pad[colji2*npad+rowji];
      mat_trans_pad[colij*npad+rowij2] = mji2;
      mat_trans_pad[colji2*npad+rowji] = mij2;

      double mij3 = mat_trans_pad[colij*npad+rowij3];
      double mji3 = mat_trans_pad[colji3*npad+rowji];
      mat_trans_pad[colij*npad+rowij3] = mji3;
      mat_trans_pad[colji3*npad+rowji] = mij3;*/

    }
}

/*
 * This auxiliary subroutine performs a smaller transpose operation on a on
 * diagonal sub matrix
 */
inline static void do_block_diagonal(int n, int npad, int block_size, int ndiag, double* mat_trans_pad)
{
  int imax=min(block_size, n - (ndiag*block_size));
  for (int i = 0; i < imax; ++i)
    for (int j = 0; j < i; ++j)
    {
      int colij=ndiag*block_size+i;
      int rowij=ndiag*block_size+j;
      int colji=ndiag*block_size+j;
      int rowji=ndiag*block_size+i;

      double mij = mat_trans_pad[colij*npad+rowij];
      double mji = mat_trans_pad[colji*npad+rowji];
      mat_trans_pad[colij*npad+rowij] = mji;
      mat_trans_pad[colji*npad+rowji] = mij;
    }
}

/*
 * Does the same thing as copy_transpose but blocked for speed
 * Blocking mechanism is stolen from Bill's code
 */
void copy_transpose_blocked (int n, int npad, double* mat, double* mat_trans_pad)
{
  // To be filled in when Bill finishes his multi-level blocking
  copy_to_pad(n,npad,mat,mat_trans_pad);
  /* For each block-row of A */
  for (int k = 0; k <= npad / BLOCK_SIZE; k++)
  {
    /* For each block-column of B */
    for (int l = 0; l <= k; l++){
        /* Transpose the block */
        do_block_non_diagonal(n, npad, BLOCK_SIZE, k, l, mat_trans_pad);
    }
  }
  for (int k = 0; k <= npad / BLOCK_SIZE; k++)
        do_block_diagonal(n, npad, BLOCK_SIZE, k, mat_trans_pad);

}

/*
 * Method to transpose data from one matrix into a padded transpose
 * Assumes non-padded matrix *mat with size n*n*sizeof(double)
 * Assumes padded matrix that is the result of the call:
 * mat_trans_pad = (double *) calloc( npad * npad * sizeof(double))
 */
void copy_transpose (int n, int npad, double* mat, double* mat_trans_pad)
{
  /* Populate the padded array */
  copy_to_pad(n,npad,mat,mat_trans_pad);
  /* Now do the transpose operation */
  for (int i = 0; i < n; ++i)
    for (int j = 0; j < i; ++j)
    {
      double mij = mat_trans_pad[i+j*npad];
      double mji = mat_trans_pad[j+i*npad];
      mat_trans_pad[j+i*npad] = mij;
      mat_trans_pad[i+j*npad] = mji;
    }
}

/*
 * The reverse
 */
void copy_from_pad(int n, int npad, double* mat, double* mat_pad) {
  int num_elems = n*sizeof(double);
  for (int i = 0; i < n; ++i) {
    memcpy(mat+(i*n), mat_pad+(i*npad), num_elems);
  }
}

/*
 * Compute the transpose operation for 4 rows using AVX
 */
inline void transpose_pd(__m256d *r1, __m256d *r2, __m256d *r3, __m256d *r4) {
  __m256d t1 = _mm256_unpacklo_pd(*r1, *r2);
  __m256d t2 = _mm256_unpackhi_pd(*r1, *r2);
  __m256d t3 = _mm256_unpacklo_pd(*r3, *r4);
  __m256d t4 = _mm256_unpackhi_pd(*r3, *r4);
  *r1 = _mm256_permute2f128_pd(t1, t3, 0x20);
  *r2 = _mm256_permute2f128_pd(t2, t4, 0x20);
  *r3 = _mm256_permute2f128_pd(t1, t3, 0x31);
  *r4 = _mm256_permute2f128_pd(t2, t4, 0x31);
}

/*
 * Efficiently transposes a 4x4 block using AVX
 */
inline void transpose_block(double *A, double *B, const int n, const int n_pad) {
  __m256d r1 = _mm256_load_pd(A);
  __m256d r2 = _mm256_load_pd(A+(n));
  __m256d r3 = _mm256_load_pd(A+(2*n));
  __m256d r4 = _mm256_load_pd(A+(3*n));
  transpose_pd(&r1, &r2, &r3, &r4);
  _mm256_store_pd(B, r1);
  _mm256_store_pd(B+(n_pad), r2);
  _mm256_store_pd(B+(2*n_pad), r3);
  _mm256_store_pd(B+(3*n_pad), r4);
}

/*
 * Efficiently transpose a matrix using AVX
 */
void block_transpose(double *A, double *B, int n, int n_pad) {
  int block_size = 16;
  int step_size = 4;
  for (int i = 0; i < n; i += block_size) {
    for (int j = 0; j < n; j += block_size) {
      int M = min(block_size, n-i);
      int N = min(block_size, n-j);
      for (int i2 = i; i2 < i+M; i2 += step_size) {
        for (int j2 = j; j2 < j+N; j2 += step_size) {
          transpose_block(A+j2+(i2*n), B+i2+(j2*n_pad), n, n_pad);
        }
      }
    }
  }
}
